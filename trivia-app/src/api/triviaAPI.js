//Fetching categories
//Does not work yet
export function getAllCategories() {
    return fetch("https://opentdb.com/api_category.php")
                .then(response => response.json())
                .then(data => data.data)
    }

//Retrieving questions
export function getQuestions(amount, category, difficulty) {
    //Somehow difficulty and category has each others value and I'm not sure why, but they are swapped now
    //On further testing it seems they all have the wrong value... will have to look into this.
    console.log(amount);
    console.log(category);
    console.log(difficulty);
    return fetch(`https://opentdb.com/api.php?amount=${difficulty}&category=${amount}&difficulty=${category}`)
        .then((response) => {
            if(response.status !== 200) {
                throw new Error("Something went wrong!");
            }

            return response.json();
        })
}