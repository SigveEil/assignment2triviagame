import Vue from 'vue'
import VueRouter from 'vue-router'
import ConfigureQuiz from './components/ConfigureQuiz.vue'
import Quiz from './components/Quiz.vue'
import ScoreBoard from './components/ScoreBoard.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/configure',
        alias: '/',
        component: ConfigureQuiz
    },
    {
        path: '/quiz',
        component: Quiz
    },
    {
        path: '/score',
        component: ScoreBoard
    }
]

export default new VueRouter({ routes })